;==================================================================================================
;��������� �����������!
;==================================================================================================
			List		p=16f628a		;����� ����
			#include	<p16f628a.inc>	;Header file
			__Config	_BODEN_OFF  & _CP_ON & DATA_CP_OFF & _PWRTE_ON & _WDT_OFF & _LVP_OFF & _MCLRE_OFF & _HS_OSC


;����������� ����� ����������-----------------------------------------------------------------------------------------
			CBLOCK		020h 	;���� ������������ ���������� (16 �����, �������� ������� ������ 020h-02Fh)
			N					;1 ����� �������, �� ����� ��������� 16
			TRC					;2 �������� ����
			STC					;3 �������� ����
			AUC					;4 �� ������������ �������� 3-�� ������
			DL					;5 ������ �������, �� ����� ���� 0
			DH					;6 ������ ��r�����, �� ����� ���� 0
			REVERS				;7 0 - ������ ����, 1 - ������ ����, 2-���������� ������������� ����, 3 ������ ����/���, 5- ���������� �������� ������
			EMERGNC				;8 ����� ������������ ��������
			RBACK				;9 ���� ������ ���������
			BACK_TMR			;10
								;11
								;12
								;13
								;14
								;15
								;16					
			ENDC

			CBLOCK		030h ;���� ����������
;---���������� ����� ��� ����, ���� � 3-�� ������---------------------
			TRL, TRDL, TRDH, TRH 	;4  ���������� ��������� ����
			STL, STDL, STDH, STH    ;8  ���������� ��������� ����
;---������� ���������� ��� ������ ����, ����, 3-�� ������-------------
			TR						;9  ������� ��������� ����
			POS_TR					;10 POS_TR,4 - ������ ��� ���������� ����������� ������� POS_TR,5 - ��������� ���������� �������� ������� POS_TR,6 - ���������� ������� ����, POS_TR,7 - ��������� �������
			ST						;11 ������� ��������� ����
			POS_ST					;12
			AU						;13 ������� ��������� 3-�� ������
			AU_L					;14 ������ ������� ���� ��������������
			AU_H					;15 ������� ������� ���� ��������������
			POS_AU					;16 ������� �����
;-------------------------------------------------------------------------
;040H
			BACK_CNT				;���������� ���������� ������� � ������� ������� ����
			ADR						;���������� ��� �������� ������ � ������ ��� ������ � eeprom	
;---��� �����������---------------------------------------------------		
			R
			R1 
			R0
			R0_CONST
			R1_CONST
			I
			I_CONST
			M
			STAT
;---��� ��� ������ ���������------------------------------------------
			RB
			R1B 
			R0B
			R0B_const
			R1B_const
;050H
			R0B_cnt_m
			R1B_cnt_m

			STPDL1			;�������� ��� ��������� ������� ����
			STPDL2
		
			BL1_CONST		;�������� ��� ���������� ���������� ������� ������� ������������
			BL2_CONST
			BL1
			BL2

			PRTA			;���������� ������ � � B
			PRTB

			TEMP			;���� ����
	
;---��������� ������� ������---------------------------------------------------------
			BUT_TMR1
			BUT_TMR2
			BUT_RES	
			N_WORK

			BL_P1_CNT
;060H
			BL_P1
			BL_P2_CNT
			BL_P2
			BL_P3_CNT
			BL_P3
	
;---�������� �������---------------------------------------------------------

			ENDC

			CBLOCK		074h ;���� ����������
			STAT10,STAT11,STAT12,STAT13
			ENDC	

;---���� ������ 1-----------------------------------------------------
			CBLOCK		0A0h ;���� ����������
			
			
;---��� ��� ����� ���� �������� �����---------------------------------

			RL
			R1L 
			R0L
			R0L_const
			R1L_const
;---��� ��� ������ ���� �������� �����--------------------------------
			RR
			R1R 
			R0R
			R0R_const
			R1R_const

;---�������� ���� ����� ���� ���--------------------------------------
			P1_CNST
			P2_CNST
			P3_CNST

			P1
			P2
			P3
		
			BLINK_N
			BLINK_OUTA
			BLINK_OUTB

			ENDC
;==================================================================================================
;������
;==================================================================================================
#define		inpT		PORTA,2		;���� TH
#define		inpS		PORTA,3		;���� ST
#define		inpA		PORTA,4		;���� AUX
#define		inpBUTT		PORTA,5		;���� ������

#define		outSTOP		PRTB,3		;����� ���� ������
#define		outBACK		PRTB,1		;����� ������ ���
#define		outGABAR	PRTB,2		;����� ������ ��������

#define		outGALOG	PRTB,0		;����� �������� ���������
#define		outBSL		PRTB,5		;����� ������� �����
#define		outDSL		PRTB,7		;����� ������� �����
#define		outBSR		PRTB,4		;����� ������� ������
#define		outDSR		PRTB,6		;����� ������� ������

#define		outLEFT		PRTA,0		;����� ����� ����������
#define		outRIGHT	PRTA,1		;����� ������ ����������


		org     2000h
		DE		00h, 00h, 40h, 40h




;==================================================================================================
;������ � ������ �������� ��� ������������ ����������
;==================================================================================================
;0  - N :��������� ������������ ������
;1  - TRC: �������� ����
;2  - STC: �������� ����
;3  - AUC: �������� 3-�� ������
;4  - DL: ������ �������
;5  - DH: ������ ��������
;6  - REVERS: ������ �������
;7  - 
;8  - 
;9  - 
;10 - 
;11 - 
;12 - 
;13 - 
;14 - 
;15 - 
;16 - SYS_OPT
;........ ........................................................................................
			org        2100h       ; ��������� � EEPROM ������ ������.
	        DE         .0,82H,81H,.30,.2,.3,b'00000000',.0,.1,.4,.0,.0,.0,.0,.0, b'00000000'
;						1  2   3    4  5  6      7       8  9  10 11 12 13 14 15 16

nop

;�������� ���� ���������==============================================================================================
			ORG		0			;������ ���������� ���������
			GOTO	START		;������� �� ����� START
;------------------------------------------------------------------------------
BACKLIGHT	CALL	BACKLIGHT1
			MOVWF	R1B_cnt_m
			movwf	R1B_const
			movwf	R1B	
			CALL	BACKLIGHT0
			MOVWF	R0B_cnt_m
			movwf	R0B_const
			movwf	R0B	
			return
		
BACKLIGHT1	MOVF	RBACK,W
			ADDWF	PCL,F
			retlw	.1
			retlw	.7
			retlw	.25
			retlw	.40
			retlw	.55
			retlw	.70
			retlw	.85
			retlw	.99

BACKLIGHT0	MOVF	RBACK,W
			ADDWF	PCL,F
			retlw	.99
			retlw	.93
			retlw	.75
			retlw	.60
			retlw	.45
			retlw	.30
			retlw	.15
			retlw	.1
;------------------------------------------------------------------------------
EFF			movf		N_WORK,W
			sublw		.24
			btfsc		STATUS,C
			GOTO		EFFF
			CLRF		N
			CLRF		N_WORK
			CALL		WRITE_MEM
			GOTO		HALT_ERR_N

EFFF		movlw		HIGH EFFF	;������ �������� ����� ������ � 
			movwf		PCLATH		;������� PCLATH
			movf		N_WORK,W			;
			addwf		PCL,F		;
			goto		EFF1		;
			goto		EFF2		;
			goto		EFF3		;
			goto		EFF4		;
			goto		EFF5		;
			goto		EFF6		;
			goto		EFF7		;
			goto		EFF8		;
			goto		EFF9		;
			goto		EFF10		;
			goto		EFF11		;
			goto		EFF12		;
			goto		EFF13		;
			goto		EFF14		;
			goto		EFF15		;
			goto		EFF16		;
			goto		EFF17		;
			goto		EFF18		;
			goto		EFF19		;
			goto		EFF20		;
			goto		EFF21		;
			goto		EFF22		;
			goto		EFF23		;
			goto		EFF24		;
			goto		EFF25		;
;------------------------------------------------------------------------------
;������ � ������� ������-------------------------------------------------------
START		bsf			STATUS,RP0	;���� 1   ����� ����� 1 
			bcf			STATUS,RP1	;���� 0
			movlw		.60			;00111100
			movwf		TRISA
			movlw		.0			;00000000 - ��� ������
			movwf		TRISB
			bcf			STATUS,RP0	;����� ����� 0
			bcf			STATUS,RP1
		
			bcf			T1CON,1		;���������� ���1
			bcf			T1CON,4		;����������� ������ ������
			bcf			T1CON,5		;������������, 1:1

			movlw		B'01001101'	;������� OPTION - 0-2: ������������ WDT 1/32 3: ������������ WDT, 7: ������������� ��������� ���������
			movwf		OPTION_REG
			
			movlw		B'00000111'	;���������� ����������� �� ����� �
			movwf		CMCON

			bcf			EECON1,WREN	;������ ������ � �����

;-----------�������� �� ���� � ����� ����������������--------------------------

			CLRF		PORTB		;������� �����, ������� ���� ��� ���������
			CLRF		PORTA
			CALL		READ_MEM

IF_PROGR	btfsS		inpBUTT		;���� ������ ������, ��  ������ � ����������������
			goto		BEGIN		;� ��������� ������ ���������� � ���� ������
			GOTO		PROGRAM
					
BUT_PRG		MOVLW		B'00001110'
			movwf		PORTB
			CLRF		PORTA
			CALL		PAUSE50
			CLRF		PORTB
			CALL		PAUSE50

			btfsc		inpBUTT
			goto		BUT_PRG			

			GOTO		MD1_TR



;��������� ����������------------------------------------------------
BEGIN		CALL	PAUSE100

			clrf	TR			;����� �������� ����
			clrf	ST			;����� �������� ����
			clrf	AU			;����� �������� 3-�� ������
			clrf	PRTA		;����� ����� �
			clrf	PRTB		;����� ����� �
			clrf	PORTA
			clrf	PORTB
			clrf	BACK_CNT	;������� ���������� ������� �� ������������ ������� ����
			clrf	RB			; ���������� ���� ���� ������ ���������
			clrf	POS_TR		;		
			clrf	POS_ST		;
			clrf	POS_AU		;
			clrf	R			;���������� ������ ������������
			clrf	BUT_RES		;������� ��������� ������
			

			movlw	.250		;�������� ��� ���������� ���������� ������� ������� ������������
			MOVwF	BL1_CONST
			movwf	BL1
			movlw	.18
			MOVwF	BL2_CONST
			movwf	BL2

			MOVLW	.0		;
			MOVWF	AU_L	;
			MOVLW	.255	;
			MOVWF	AU_H	;

			movlw	.20		;������� �������
			movwf	M		;
			
			movlw	.255	;
			movwf	EMERGNC	;

			movlw	.255
			movwf	BUT_TMR1
			movlw	.40
			movwf	BUT_TMR2


			movlw	.255
			movwf	BL_P1_CNT
			movwf	BL_P1
			movlw	.100
			movwf	BL_P2_CNT
			movwf	BL_P2
			movlw	.6
			movwf	BL_P3_CNT
			movwf	BL_P3


			movlw	.255		
			movwf	STPDL1				
			movlw	.130			
			movwf	STPDL2

;������ ��������-----------------------------------------------------
			call	READ_MEM
			movf	N,W
			movwf	N_WORK

			MOVLW	.1
			movwf	RBACK
			call	BACKLIGHT


			call	EFF
	
;��������� �������� �����--------------------------------------------
			bcf		POS_TR,7
			bcf		POS_ST,7
			bcf		POS_AU,7
			bcf		POS_AU,6
			bcf		POS_AU,0
			bsf		POS_TR,1
			bsf		POS_ST,1			
			bcf		POS_AU,5
			clrf	TR
			CLRF	ST

;������� ����======================================================================================
BEGIN1		call	CNT_1		;	(0)
;������ ����-(20)-------------------------------------
			btfss	POS_TR,7		;(26)
			goto	NO_TR_ANAL		;(27)
			movf	TRDH,W
			subwf	TR,W
			btfss	STATUS,C
			goto	tr_step1		;(31)

tr_step2	movf	TRH,W			;(32)
			subwf	TR,W
			btfss	STATUS,C
			goto	tr_nothin1		;(35)
			goto	tr_hi			;(36)

tr_step1	movf	TRL,W			;(33)
			subwf	TR,W
			btfss	STATUS,C
			goto	tr_low			;(36)
			movf	TRDL,W
			subwf	TR,W
			btfss	STATUS,C
			goto	tr_nothin2		;(40)
			goto	tr_neutral		;(41)
;---------------------------------------------------
tr_nothin1	CLRF	TR				;(37)
			BCF		POS_TR,7
			call	PSE4
			call	PSE5
			GOTO	END1			;(48)	
;---------------------------------------------------
tr_hi		bcf		POS_TR,0		;(38)
			bcf		POS_TR,1
			bsf		POS_TR,2
			clrf	TR
			bcf		POS_TR,7
			call	PSE5
			GOTO	END1			;(48)
;---------------------------------------------------
tr_low		bsf		POS_TR,0		;(38)
			bcf		POS_TR,1
			bcf		POS_TR,2
			clrf	TR
			bcf		POS_TR,7
			call	PSE5
			GOTO	END1			;(48)
;---------------------------------------------------
tr_nothin2	CLRF	TR				;(42)
			BCF		POS_TR,7
			call	PSE4
			GOTO	END1			;(48)
;---------------------------------------------------
tr_neutral	bcf		POS_TR,0		;(43)
			bsf		POS_TR,1
			bcf		POS_TR,2
			CLRF	TR
			BCF		POS_TR,7
			GOTO	END1			;(48)
;---------------------------------------------------
NO_TR_ANAL	call	PSE4			;(29)
			call	PSE5			;(33)
			call	PSE10
			GOTO	END1			;(48)
;---------------------------------------------------
END1		CALL	PSE5			;(50)
			CALL	PSE4			;(55)
			MOVF	PRTB,W			;(59)
			MOVWF	PORTB			;(60)
			

;===========================================================================================================================		
BEGIN2		call	CNT_2			;(29)
;---------------------------------------------------
			BTFSC	REVERS,0		;(30) ����������� �������
			goto	W_TRREV			;(31)		
;---------------------------------------------------
WO_TRREV	nop						;(32)
			BTFSC	POS_TR,0
			GOTO	STOP			;(34)
			BTFSC	POS_TR,2
			GOTO	FRW				;(36)
			GOTO	NEUTRAL			;(37)
;---------------------------------------------------
W_TRREV 	BTFSC	POS_TR,2		;(33)
			GOTO	STOP			;(34)
			BTFSC	POS_TR,0
			GOTO	FRW				;(36)
			GOTO	NEUTRAL			;(37)
;---���������� ������� � ������ �����--------------------------------
STOP		BTFSC	POS_TR,6		;(36)
			goto	MOVBACK			;(37)
			movlw	.99				;������ �� 100% ��������
			movwf	R1B_const
			movlw	.1
			movwf	R0B_const
			bsf		outSTOP			;������ �����
			bcf		outBACK
			btfsc	POS_TR,5		;��������� �� ���������� ����������
			incf	BACK_CNT,F		;����������
			bcf		POS_TR,5		;������ ����������
			movlw	.2				;(47)
			subwf	BACK_CNT,W
			btfsc	STATUS,Z
			bsf		POS_TR,6
			goto	END2			;(51)
;--------------------------------------------------------------------
MOVBACK		movf	R1B_cnt_m,W		;(39)   ������ �� 10% ��������
			movwf	R1B_const
			movf	R0B_cnt_m,W
			movwf	R0B_const
			bsf		outBACK			;������ �����
			bcf		outSTOP
			bsf		POS_TR,6
			call	PSE4
			NOP
			goto	END2			;(51)			
;-------------------------------------------------------------------
FRW			clrf	BACK_CNT		;(38)
			movlw	.255
			movwf	STPDL1
			movlw	.80
			movwf	STPDL2
			movf	R1B_cnt_m,W				;������ �� 10% ��������
			movwf	R1B_const
			movf	R0B_cnt_m,W
			movwf	R0B_const
			bcf		outBACK	
			bcf		outSTOP
			bcf		POS_TR,6
			NOP
			goto	END2			;(51)			
;--------------------------------------------------------------------
NEUTRAL		movf	R1B_cnt_m,W		;(39)  ������ �� 10% ��������
			movwf	R1B_const
			movf	R0B_cnt_m,W
			movwf	R0B_const
			bcf		outBACK	
			bcf		outSTOP
			bsf		POS_TR,5
			decfsz	STPDL1				
			goto	NEUTRAL1		;(47)
			DECFSZ	STPDL2			;(48)
			goto	B2_1			;(49)
			bsf		POS_TR,6
			goto	END2			;(51)
	
NEUTRAL1	NOP						;(49)
			NOP	
			goto	END2			;(51)
;--------------------------------------------------------------------			
B2_1		NOP						;(51)
			nop						;(52)
END2		nop						;(53)
			CALL	PSE5			;(54)
			MOVF	PRTB,W			;(59)
			MOVWF	PORTB			;(60)
		
;===========================================================================================================================
BEGIN3		CALL	CNT_3
			CALL	PSE10			;(18)
;--------------------------------------------------------------------
END3		call	BUTT			;(28)
			BTFSC	BUT_RES,0		;(43)		
			bcf		POS_ST,3
			BTFSC	BUT_RES,0		;(45)			
			INCF	N,F
			movf	N,W
			sublw	.24
			btfss	STATUS,C
			CLRF	N
			BTFSC	BUT_RES,1				
			CLRF	N				;(52)
			movf	N,W
			movwf	N_WORK			;(54)
			BTFSC	BUT_RES,0					
			bsf		POS_AU,0
			nop						;(57)
			nop						;(58)		
			MOVF	PRTB,W			;(59)
			MOVWF	PORTB			;(60)

;===========================================================================
BEGIN4		CALL	CNT_1
			
			btfss	POS_ST,7		;(26)
			goto	NO_ST_ANAL		;(27)

			movf	STDH,W
			subwf	ST,W
			btfss	STATUS,C
			goto	st_step1		;(31)
;--------------------------------------------------------------------
st_step2	movf	STH,W			;(32)
			subwf	ST,W
			btfss	STATUS,C
			goto	st_nothin1		;(35)
			goto	st_hi			;(36)
;--------------------------------------------------------------------
st_step1	movf	STL,W			;(33)
			subwf	ST,W
			btfss	STATUS,C
			goto	st_low			;(36)
			movf	STDL,W
			subwf	ST,W
			btfss	STATUS,C
			goto	st_nothin2		;(40)
			goto	st_neutral		;(41)
;--------------------------------------------------------------------
st_nothin1	CLRF	ST				;(37)
			BCF		POS_ST,7
			call	PSE10			;(39)
			nop						;(49)
			GOTO	END4			;(50)
;--------------------------------------------------------------------
st_hi		btfsc	REVERS,1		;(38)
			GOTO	st_hwrev
st_hworev	nop
			bcf		POS_ST,0		;(41)
			bcf		POS_ST,1
			bsf		POS_ST,2
			CLRF	ST
			BCF		POS_ST,7		;(45)
			call	PSE4			;(46)
			GOTO	END4			;(50)


st_hwrev	bsf		POS_ST,0		;(41)
			bcf		POS_ST,1
			bcf		POS_ST,2
			CLRF	ST
			BCF		POS_ST,7		;(45)
			call	PSE4			;(46)
			GOTO	END4			;(50)
;----------------------------------------
;������ ����� ���� ��� �������
;			bcf		POS_ST,0		;(38)
;			bcf		POS_ST,1
;			bsf		POS_ST,2
;			CLRF	ST
;			BCF		POS_ST,7
;			call	PSE6
;			nop
;			GOTO	END4			;(50)

;--------------------------------------------------------------------
st_low		btfsc	REVERS,1		;(38)
			GOTO	st_lwrev
st_lworev	nop
			bsf		POS_ST,0		;(41)
			bcf		POS_ST,1
			bcf		POS_ST,2
			CLRF	ST
			BCF		POS_ST,7		;(45)
			call	PSE4			;(46)
			GOTO	END4			;(50)


st_lwrev	bcf		POS_ST,0		;(41)
			bcf		POS_ST,1
			bsf		POS_ST,2
			CLRF	ST
			BCF		POS_ST,7		;(45)
			call	PSE4			;(46)
			GOTO	END4			;(50)

;---------------------------------------------------
;������ ����� ���� ��� �������
;			bsf		POS_ST,0		;(38)
;			bcf		POS_ST,1
;			bcf		POS_ST,2
;			CLRF	ST
;			BCF		POS_ST,7
;			call	PSE6
;			nop
;			GOTO	END4			;(50)
;--------------------------------------------------------------------
st_nothin2	CLRF	ST				;(42)
			BCF		POS_ST,7
			call	PSE6
			GOTO	END4			;(50)
;--------------------------------------------------------------------
st_neutral	bcf		POS_ST,0		;(43)		
			bsf		POS_ST,1
			bcf		POS_ST,2
			CLRF	ST
			BCF		POS_ST,7
			btfss	POS_ST,3
			bcf		POS_ST,6
			GOTO	END4			;(50)
;--------------------------------------------------------------------
NO_ST_ANAL	call	PSE10			;(29)
			call	PSE10			;(39)
			nop						;(49)
			GOTO	END4			;(50)
;--------------------------------------------------------------------
END4		call	PSE6			;(52)
			NOP
			MOVF	PRTB,W			;(59)
			MOVWF	PORTB			;(60)
			
;===========================================================================
BEGIN5		CALL	CNT_2			;(29)
			movf	EMERGNC,W		;(30)
			btfsc	STATUS,Z
			NOP						;(32)		
			
			btfss	POS_ST,1		;(33)
			goto	B5_1			;(34)
			btfss	POS_TR,1
			goto	B5_2			;(36)
;----------------------------------------
			btfsc	POS_ST,3		
			goto	B5_3			;(38)
;----------------------------------------
		
			decfsz	BL_P1
			GOTO	B5_4			;(40)
			MOVF	BL_P1_CNT,W
			movwf	BL_P1

			decfsz	BL_P2
			GOTO	B5_5			;(44)
			MOVF	BL_P2_CNT,W
			movwf	BL_P2

			decfsz	BL_P3
			GOTO	B5_6			;(48)
			MOVF	BL_P3_CNT,W
			movwf	BL_P3
			bcf		STATUS,RP0
			bsf		POS_ST,3
			GOTO	END5			;(53)
;----------------------------------------
B5_1		NOP						;(36)
			NOP
B5_2		bcf		POS_ST,3		;(38)
			MOVF	BL_P1_CNT,W		
			movwf	BL_P1						
			MOVF	BL_P2_CNT,W
			movwf	BL_P2
			MOVF	BL_P3_CNT,W
			movwf	BL_P3				
			GOTO	B5_7			;(45)
;----------------------------------------

B5_3		nop						;(40)
			nop	
B5_4		nop						;(42)
			nop
			nop
			nop
B5_5		nop						;(46)
B5_7		nop						;(47)
			nop
			nop
B5_6		nop						;(50)
			nop
			nop			
			nop
			NOP						;(54)
	
END5		nop						;(55)
			nop
			NOP
			nop
			MOVF	PRTB,W			;(59)
			MOVWF	PORTB			;(60)

;===========================================================================
BEGIN6		CALL	CNT_3			;
			
			btfsc	POS_ST,3		;(18)
			goto	AVAR			;(19)
			NOP
			NOP
			goto	NORMAL			;(22)
;------------------------------------------------------------

AVAR		BSF		POS_ST,0		;(21)
			BSF		POS_ST,2		;(22)
			BSF		POS_ST,1		;(23)

			GOTO	WO_STREV		;(24)
;------------------------------------------------------------
NORMAL		BTFSC	POS_ST,1		;(24)
			GOTO	CENTRAL			;(25)
;------------------------------------------------------------
WO_STREV	btfsc	POS_ST,6		;(26)
			goto	WO_STREV_0		;(27)
;============================================================
WO_STREV_1	Btfsc	POS_ST,0		;(28)
			BSF		outLEFT	
			btfsc	POS_ST,2
			BSF		outRIGHT
			decfsz	BL1,F
			goto	B6exit1			;(33)
			movf	BL1_CONST,W
			movwf	BL1
			decfsz	BL2
			goto	B6exit2			;(37)
			movf	BL2_CONST,W
			movwf	BL2
			bsf		POS_ST,6
			goto	B6exit3			;(41)
;------------------------------------------------------------
WO_STREV_0	nop						;(29)
			BCF		outLEFT			
			BCF		outRIGHT
			decfsz	BL1
			goto	B6exit1			;(33)
			movf	BL1_CONST,W
			movwf	BL1
			decfsz	BL2
			goto	B6exit2			;(37)
			movf	BL2_CONST,W
			movwf	BL2
			bcf		POS_ST,6
			goto	B6exit3			;(41)

;------------------------------------------------------
CENTRAL		bcf		outLEFT			;(27)
			bcf		outRIGHT
			MOVF	BL1_CONST,w
			movwf	BL1
			MOVF	BL2_CONST,w
			movwf	BL2		;(32)
			nop
			nop
B6exit1		nop				;(35)
			nop
			nop
			nop				
B6exit2		nop				;(39)
			nop
			nop
			nop				
B6exit3		nop				;(43)
			CALL	PSE10	;(44)
			NOP				;(54)
			BTFSC		BUT_RES,3
			GOTO		BUT_PRG
END6		MOVF	PRTA,W		;(57)
			MOVWF	PORTA		;
			MOVF	PRTB,W		;
			MOVWF	PORTB		;(60)
		
					
;------------------------------------------------------------------------------------------------------------------------------------------------------

BEGIN7		call	CNT_1			
			movlw	b'00001111'	;(26)
			andwf	PRTB,F
			movlw	b'11110000'
			andwf	STAT,W
			iorwf	PRTB,F
	
			DECFSZ	M,F
			GOTO	B7_1		;(32)
			MOVLW	.20
			MOVWF	M

			DECFSZ	R1,F
			GOTO	B7_2		;(36)
			MOVF	R1_CONST,W
			MOVWF	R1

			SWAPF	STAT,F
			
			DECFSZ	I,F
			goto	B7_3		;(41)
			MOVF	I_CONST,W
			MOVWF	I			
			
			INCF	R,F			;(44)
			movf	R,W
			sublw	.4
			btfsc	STATUS,Z
			clrf	R	
			
			movf	R,W			;(49)	
			addlw	74H		
			MOVWF	FSR		
			movf	INDF,W
			movwf	STAT		
			goto	END7		;(54)

B7_1		NOP					;(34)
			NOP
			NOP
			NOP
B7_2		NOP					;(38)
			NOP
			NOP
			NOP
			NOP
B7_3		NOP					;(43)
			BTFSC	POS_ST,4
			BSF		BUT_RES,0
			BTFSC	POS_ST,4
			bcf		POS_ST,4
			BTFSC	POS_ST,3
			BSF		POS_ST,4
			btfsc	POS_ST,3
			CLRF	N_WORK	
			btfsc	POS_ST,3
			BSF		BUT_RES,0
			NOP
			NOP

END7		NOP						;(56)
			NOP
			NOP		
			MOVF	PRTB,W			;(59)
			MOVWF	PORTB			;(60)


;-----------------------------------------------------------------------------------------------
;��������� �������� ������
BEGIN8		call	CNT_2				
			
			
			btfss	POS_AU,7			;(30)
			goto	B8_NOP				;(31)

			BTFSC	REVERS,5			;(32)
			goto	B8_NOP1				;(33)

;----------------------------------------------
		
			btfsc	POS_AU,6			;(34)
			goto	B8_prc1				;(35)

B8_prc2		movf	AU,W				;(36)
			subwf	AU_L,W
			btfsc	STATUS,C
			goto	B8_prc21			;(39)
			movf	AU,W
			subwf	AU_H,W
			btfss	STATUS,C
			goto	B8_prc22			;(43)
			bsf		POS_AU,6
			goto	B8_out1				;(45)
			
B8_prc21	nop							;(41)
			nop
			nop
			nop
B8_prc22	bsf		BUT_RES,0			;(45)
			btfsc	POS_AU,5
			bsf		BUT_RES,2
			bsf		POS_AU,6	
			bsf		POS_AU,5
			bcf		POS_ST,3
			goto	B8_out2				;(51)
;----------------------------------------------

B8_prc1		movf	AU,W				;(37)
			movwf	AU_L
			movlw	.5
			SUBWF	AU_L,F
	
			movf	AU,W				;(41)
			movwf	AU_H
			movlw	.5
			addwf	AU_H,F
			bcf		POS_AU,6
			bcf		POS_AU,7
			clrf	AU
			GOTO	B8_out3				;(48)
			

;----------------------------------------------
B8_NOP		NOP							;(33)
			NOP
B8_NOP1		NOP							;(35)
			nop
			NOP
			NOP
			NOP
			NOP
			NOP
			NOP
			NOP
			NOP
			NOP
			NOP
B8_out1		NOP							;(47)
			NOP
			NOP
B8_out3		NOP							;(50)						
			NOP
			NOP							;(52)
B8_out2			NOP							
			NOP
			NOP
			NOP
			NOP
			NOP
END8		MOVF		PRTB,W			;(59)
			MOVWF		PORTB			;(60)



;-----------------------------------------------------------------------------------------------
BEGIN9		call	CNT_3

			BTFSC	BUT_RES,0			;(18)
			goto	NEW_EFF				;(19)

			btfsc	POS_AU,0
			goto	MEM_EFF				;(21)

B9_FREE		CALL	PSE10				;(22)
			CALL	PSE10				;(32)
			CALL	PSE10				;(42)
			nop
			goto	END9				;(53)	



MEM_EFF		bcf			POS_AU,0		;(23)
			bsf			STATUS,RP0		;����� ����� 1
			bsf			EECON1,WREN
			movlw		.0
			movwf		EEADR		
			bcf			STATUS,RP0
			movf		N,W
			bsf			STATUS,RP0
			movwf		EEDATA
			movlw		55H
			movwf		EECON2
			movlw		0AAH
			movwf		EECON2
			bsf			EECON1,WR
			bcf			STATUS,RP0		;(37)
			CALL		PSE10			;(38)
			CALL		PSE5			;(48)
			goto		END9			;(53)	


NEW_EFF		BSF		POS_AU,0			;(21)
			call	EFF					;(22)
			
		
	
			
END9		NOP							;(55)
			
			MOVF		PRTB,W			;(56)
			MOVWF		PORTB			;(57)
			GOTO		BEGIN1			;(58)

;-----------------------------------------------------------------------------------------------------------------




HALT_ERR_N	clrf		PORTB
			clrf		PORTA
			bsf			outGALOG
			bsf			outGABAR
HALT_ERR_N1	movf		PRTB
			movwf		PORTB
			goto		BEGIN1
			GOTO		HALT_ERR_N1	

;-----------------------------------------------------------------------------------------------

;========================================================================================================================================================================
;========================================================================================================================================================================
;========================================================================================================================================================================
;========================================================================================================================================================================
;========================================================================================================================================================================
;========================================================================================================================================================================

;---������� ������������ 1 (36)------------------------------------------------
CNT_1		btfsc	inpT			; ���� �� ����� ������������ 1
			goto	T_CNT1_OFF		; �� ���� � ������� ������������
T_CNT1_ON	incf	TR				; ���� LastTr ������ 0, �� ����������� ������� ��������
			NOP
			NOP
			NOP
			goto	CNT1_S		; ����� ������
T_CNT1_OFF	movf	TR,W
			btfss	STATUS,Z			
			bsf		POS_TR,7		; �� ��������� ������ ������������ ��������	
			goto	CNT1_S		;	
;---------------------------
CNT1_S		btfsc	inpS			; ���� �� ����� ������������ 1
			goto	S_CNT1_OFF		; �� ���� � ������� ������������
S_CNT1_ON	incf	ST				; ���� LastTr ������ 0, �� ����������� ������� ��������
			NOP
			NOP
			NOP
			goto	CNT1_A			; ����� ������
S_CNT1_OFF	movf	ST,W
			btfss	STATUS,Z			
			bsf		POS_ST,7		; �� ��������� ������ ������������ ��������	
			goto	CNT1_A			;	
;---------------------------
CNT1_A		btfsc	inpA			; ���� �� ����� ������������ 1
			goto	A_CNT1_OFF		; �� ���� � ������� ������������
A_CNT1_ON	incf	AU				; ���� LastTr ������ 0, �� ����������� ������� ��������
			NOP
			NOP
			NOP
			RETURN
A_CNT1_OFF	movf	AU,W
			btfss	STATUS,Z			
			bsf		POS_AU,7		; �� ��������� ������ ������������ ��������	
			RETURN
;------------------------------------------------------------------------------

;---������� ������������ 2 (36)------------------------------------------------
CNT_2		btfsc	inpT			; ���� �� ����� ������������ 1
			goto	T_CNT2_OFF		; �� ���� � ������� ������������
T_CNT2_ON	incf	TR				; ���� LastTr ������ 0, �� ����������� ������� ��������
			NOP
			NOP
			NOP
			goto	CNT2_S		; ����� ������
T_CNT2_OFF	movf	TR,W
			btfss	STATUS,Z			
			bsf		POS_TR,7		; �� ��������� ������ ������������ ��������	
			goto	CNT2_S		;	
;---------------------------
CNT2_S		btfsc	inpS			; ���� �� ����� ������������ 1
			goto	S_CNT2_OFF		; �� ���� � ������� ������������
S_CNT2_ON	incf	ST				; ���� LastTr ������ 0, �� ����������� ������� ��������
			NOP
			NOP
			NOP
			goto	SHIMB			; ����� ������
S_CNT2_OFF	movf	ST,W
			btfss	STATUS,Z			
			bsf		POS_ST,7		; �� ��������� ������ ������������ ��������	
			goto	SHIMB	
;---------------------------
SHIMB		btfsc	RB,0
			goto	PHASEB_1

PHASEB_0	bcf		outGABAR
			decfsz	R0B
			goto	PHASEB_0_1
			movlw	.1
			movwf	RB
			movf	R0B_const,W
			movwf	R0B
			RETURN

PHASEB_0_1	NOP	
			NOP	
			NOP	
			NOP	
			RETURN

PHASEB_1	bsf		outGABAR
			decfsz	R1B
			goto	PHASEB_1_1
			movlw	.0
			movwf	RB
			movf	R1B_const,W
			movwf	R1B
			RETURN

PHASEB_1_1	NOP	
			NOP	
			NOP	
			NOP	
			RETURN
;------------------------------------------------------------------------------	

;---������� ������������ 3 (36)------------------------------------------------
CNT_3		btfsc	inpT			; ���� �� ����� ������������ 1
			goto	T_CNT3_OFF		; �� ���� � ������� ������������
T_CNT3_ON	incf	TR				; ���� LastTr ������ 0, �� ����������� ������� ��������
			NOP
			NOP
			NOP
			goto	CNT3_S			; ����� ������
T_CNT3_OFF	movf	TR,W
			btfss	STATUS,Z			
			bsf		POS_TR,7		; �� ��������� ������ ������������ ��������	
			goto	CNT3_S			;	
;---------------------------
CNT3_S		btfsc	inpS			; ���� �� ����� ������������ 1
			goto	S_CNT3_OFF		; �� ���� � ������� ������������
S_CNT3_ON	incf	ST				; ���� LastTr ������ 0, �� ����������� ������� ��������
			NOP
			NOP
			NOP
			RETURN			; ����� ������
S_CNT3_OFF	movf	ST,W
			btfss	STATUS,Z			
			bsf		POS_ST,7		; �� ��������� ������ ������������ ��������	
			RETURN			; ����� ������
;||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||	


;--------------------------------------------------------------------------------------------------
;������������ ������ �������� �� ������------------------------------------------------------------
READ_MEM	clrf		ADR	
RD_CYCLE	movf		ADR,W
			addlw		20h
			movwf		FSR
			movf		ADR,W
			bsf			STATUS,RP0	;����� ����� 1
			bcf			STATUS,RP1
			movwf		EEADR
			bsf			EECON1,RD
			movf		EEDATA,W
			bcf			STATUS,RP0	;����� ����� 1
			bcf			STATUS,RP1
			movwf		INDF
			incf		ADR
			movlw		.12
			subwf		ADR,W
			btfss		STATUS,Z
			goto		RD_CYCLE
;���������� ��������� TR------------------------------------------------			
		

		;	INCF		TRC,F

			movf		TRC,W
			movwf		TEMP
			MOVF		DL,W
			SUBWF		TEMP,W
			MOVWF		TRDL
			INCF		TRDL,F

			movf		TRC,W
			movwf		TEMP
			MOVF		DH,W
			SUBWF		TEMP,W
			MOVWF		TRL
			INCF		TRL,F

			movf		TRC,W
			movwf		TEMP
			MOVF		DL,W
			ADDWF		TEMP,W
			MOVWF		TRDH
			decf		TRDH


			movf		TRC,W
			movwf		TEMP
			MOVF		DH,W
			ADDWF		TEMP,W
			MOVWF		TRH
			DECF		TRH

;���������� ��������� ST------------------------------------------------
		
		;	decf		STC,F

			movf		STC,W
			movwf		TEMP
			MOVF		DL,W
			SUBWF		TEMP,W
			MOVWF		STDL
			incf		STDL,F

			movf		STC,W
			movwf		TEMP
			MOVF		DH,W
			SUBWF		TEMP,W
			MOVWF		STL
			incf		STL,F

			movf		STC,W
			movwf		TEMP
			MOVF		DL,W
			ADDWF		TEMP,W
			MOVWF		STDH
		;	decf		STDH

			movf		STC,W
			movwf		TEMP
			MOVF		DH,W
			ADDWF		TEMP,W
			MOVWF		STH
			;decf		STH
		
READ_END	RETURN

;---��������---------------------------------------------------------------------

	



;---------------------------------------------------------


WRITE_MEM	clrf		ADR
WR_CYCLE	clrwdt
			CALL		PAUSE5
			movlw		20h
			addwf		ADR,W
			movwf		FSR
		
			movf		ADR,W
			bsf			STATUS,RP0	;����� ����� 1
			bsf			EECON1,WREN

			movwf		EEADR
			movf		INDF,W
			movwf		EEDATA


			movlw		55H
			movwf		EECON2
			movlw		0AAH
			movwf		EECON2
			bsf			EECON1,WR

			bcf			STATUS,RP0	;����� ����� 1

			incf		ADR
			movlw		.12
			subwf		ADR,W
			btfss		STATUS,Z
			goto		WR_CYCLE
WRITE_END		RETURN		


;������������ �������� �� 500 ��-------------------------------------------------------------------
PAUSE500	bsf			STATUS,RP0
			movlw		.250
			movwf		P1_CNST
			movlw		.30
			movwf		P2_CNST
			movlw		.110
			movwf		P3_CNST
			movf		P1_CNST,W
			movwf		P1
P500_1		movf		P2_CNST,W
			movwf		P2
P500_2		movf		P3_CNST,W
			movwf		P3
P500_3		DECFSZ		P3
			goto		P500_3
			DECFSZ		P2
			goto		P500_2
			DECFSZ		P1
			goto		P500_1
			bcf			STATUS,RP0
			RETURN

PAUSE100	bsf			STATUS,RP0
			movlw		.50
			movwf		P1_CNST
			movlw		.30
			movwf		P2_CNST
			movlw		.110
			movwf		P3_CNST
			movf		P1_CNST,W
			movwf		P1
P100_1		movf		P2_CNST,W
			movwf		P2
P100_2		movf		P3_CNST,W
			movwf		P3
P100_3		DECFSZ		P3
			goto		P100_3
			DECFSZ		P2
			goto		P100_2
			DECFSZ		P1
			goto		P100_1
			bcf			STATUS,RP0
			RETURN



PAUSE50		bsf			STATUS,RP0
			movlw		.25
			movwf		P1_CNST
			movlw		.30
			movwf		P2_CNST
			movlw		.110
			movwf		P3_CNST
			movf		P1_CNST,W
			movwf		P1
P50_1		movf		P2_CNST,W
			movwf		P2
P50_2		movf		P3_CNST,W
			movwf		P3
P50_3		DECFSZ		P3
			goto		P50_3
			DECFSZ		P2
			goto		P50_2
			DECFSZ		P1
			goto		P50_1
			bcf			STATUS,RP0
			RETURN

;---------------------------------------------------------
PAUSE5		bsf			STATUS,RP0
			movlw		.7
			movwf		P1_CNST
			movlw		.15
			movwf		P2_CNST
			movlw		.110
			movwf		P3_CNST
			movf		P1_CNST,W
			movwf		P1
P5_1		movf		P2_CNST,W
			movwf		P2
P5_2		movf		P3_CNST,W
			movwf		P3
P5_3		DECFSZ		P3
			goto		P5_3
			DECFSZ		P2
			goto		P5_2
			DECFSZ		P1
			goto		P5_1
			bcf			STATUS,RP0
			RETURN
;---------------------------------------------------------
PAUSE48		bsf			STATUS,RP0
			movlw		.15
			movwf		P1_CNST
P48_1		decfsz		P1_CNST
			goto		P48_1
			nop
			nop
			nop
		;	nop
			nop
			bcf			STATUS,RP0
			RETURN
;---------------------------------------------------------
PAUSE100N	bsf			STATUS,RP0
			movlw		.150
			movwf		P1_CNST
P100N_1		decfsz		P1_CNST
			goto		P100N_1
			nop
			bcf			STATUS,RP0
			RETURN
;--------------------------------------------------------
PSE4		RETURN

PSE5		NOP
			RETURN

PSE6		NOP
			NOP
			RETURN

PSE10		bsf			STATUS,RP0
			movlw		.1
			movwf		P1_CNST
PSE10_1		decfsz		P1_CNST
			goto		PSE10_1
			bcf			STATUS,RP0
			RETURN




;��������� ������----------------------------------------------------------------------------------

BUTT		btfsc		inpBUTT		;(1) �������� �������
			goto		PRESS1		;	���� ����, �� ���� � ��������� ����� 1
;-----------------------------------------------------------
PRESS0		btfss		BUT_RES,2	;(3)�������� ������� � ������� �����
			goto		NO_PRESS	;���� �� ����, �� ���� � ��� �����
			bcf			BUT_RES,2	;���� ����, �� ��������� ������� ������� � ������� �����
			btfsc		BUT_RES,3   ; ������� ����������� �������� ������������ �������
			bsf			BUT_RES,1	;���� ���� - ������������� �������� ���
			bsf			BUT_RES,0	; ������ ��� ��������� �������
			bcf			BUT_RES,3	;��������� ��� ������������ ��������
			NOP
			NOP
			RETURN			;(12)
;-----------------------------------------------
NO_PRESS	movlw		.255		;(6)�������������� ��������, ��������� �������
			movwf		BUT_TMR1
			movlw		.40
			movwf		BUT_TMR2
			CLRF		BUT_RES
			NOP
			RETURN			;(12)
;-----------------------------------------------------------
PRESS1		bsf			BUT_RES,2	;(4)
			btfsc		BUT_RES,3
			goto		LPRESS		;(6)
			decfsz		BUT_TMR1		
			goto		BUT_OUT1	;(8)
			decfsz		BUT_TMR2
			goto		BUT_OUT2	;(10)
			bsf			BUT_RES,3
			RETURN			;(12)
;-----------------------------------------------
LPRESS		nop			;(8)
			NOP
BUT_OUT1	nop			;(10)
			nop
BUT_OUT2	RETURN			;(12)



















	














;------------------------------------------------------------------------------------------------------------------------------

;------------------------------------------------------------------------------------------------------------------------------
BREAK_1		CLRF		N			;���� N ������� �� �������,
			CALL		WRITE_MEM	;�� ������� N
			goto		START		;


;------------------------------------------------------------------------------------------------------------------------------
BLINK		bsf			STATUS,RP0	
			decfsz		BLINK_N
			goto		BLNK1
			goto		BLINK_END
BLNK1		movf		BLINK_OUTA,W
			bcf			STATUS,RP0	
			movwf		PORTA
			bsf			STATUS,RP0	
			movf		BLINK_OUTB,W
			bcf			STATUS,RP0	
			movwf		PORTB
			call		PAUSE100
			CLRF		PORTA	
			clrf		PORTB
			call		PAUSE100
			goto		BLINK
BLINK_END	bcf			STATUS,RP0	
			RETURN



;����� ����������������==============================================================================================================


PROGRAM		movlw		.10					;
			movwf		P1_CNST				;
PRGCNT_2	movlw		.255;				;
			movwf		P2_CNST				;
PRGCNT_1	movlw		.255				;
			movwf		P3_CNST	
			
		

PRG1		CLRF		PORTA
			MOVLW		B'00001100'
			movwf		PORTB

			btfss		inpBUTT
			goto		REVTH
				
			DECFSZ		P3_CNST
			goto		PRG1
			DECFSZ		P2_CNST
			goto		PRGCNT_1
			DECFSZ		P1_CNST
			goto		PRGCNT_2
			

PROGRAM2	movlw		.10					;
			movwf		P1_CNST				;
PRGCNT_4	movlw		.255;				;
			movwf		P2_CNST				;
PRGCNT_3	movlw		.255				;
			movwf		P3_CNST	


PRG2		CLRF		PORTB
			MOVLW		B'00000011'
			movwf		PORTA

			btfss		inpBUTT
			goto		REVST		
				
			DECFSZ		P3_CNST
			goto		PRG2
			DECFSZ		P2_CNST
			goto		PRGCNT_3
			DECFSZ		P1_CNST
			goto		PRGCNT_4
			GOTO		PROGRAM



REVTH		BTFSC		REVERS,0
			GOTO		REVTH1
			BSF			REVERS,0
			CALL		WRITE_MEM
			GOTO		MD1_TR


REVTH1		BCF			REVERS,0
			CALL		WRITE_MEM
			GOTO		MD1_TR

REVST		BTFSC		REVERS,1
			GOTO		REVST1
			BSF			REVERS,1
			CALL		WRITE_MEM
			GOTO		MD1_TR


REVST1		BCF			REVERS,1
			CALL		WRITE_MEM
			GOTO		MD1_TR

;---------------------------------
MD1_TR		clrf		PORTA
			CLRF		PORTB
			call		PAUSE500
			call		PAUSE500
			call		PAUSE500


			CLRF		TR					;
			bsf			STATUS,RP0			;
			movlw		.6					;
			movwf		P1_CNST				;
MD1TR_P1	movlw		.255;				;
			movwf		P2_CNST				;
MD1TR_P2	movlw		.255				;
			movwf		P3_CNST				;
;---------------------------------
MD1TR_P3	bcf			STATUS,RP0			;���� �� ����� 1
			BTFSs		inpT				;�� ���� �� ���������� ������
			goto		MD1_WT1				; �  ��������� ������
			bsf			STATUS,RP0			;�������� ������ �� ������
			decfsz		P3_CNST				;
			goto		MD1TR_P3			;
			DECFSZ		P2_CNST				;
			goto		MD1TR_P2			;
			DECFSZ		P1_CNST				;
			goto		MD1TR_P1			;
			bcf			STATUS,RP0			;
			GOTO		MD1_T_ER			;���� ������ �������� - ���� � ������������ ������
;---------------------------------------------------------			
MD1_WT1		bcf			STATUS,RP0
			BTFSs		inpT
			goto		MD1_WT1
MD1_WT2		BTFSc		inpT
			goto		MD1_WT2
;---------------------------------------------------------
MD1_CNTT	btfsc		inpT			; ���� �� ����� ������������ 1
			goto		MD1_CNTT0		; �� ���� � ������� ������������
			incf		TR				; ���� LastTr ������ 0, �� ����������� ������� ��������
			CALL		PAUSE48
			GOTO		MD1_CNTT
;---------------------------------------------------------
MD1_CNTT0	movf		TR,W
			movwf		TRC
			call		WRITE_MEM
			call		PAUSE100
			CALL		READ_MEM
			movlw		b'00001100'
			movwf		PORTB
			CALL		PAUSE500
			CALL		PAUSE500
;============================================================
MD1_ST		CLRF		ST					;
			clrf		PORTB				;
			bsf			STATUS,RP0			;
			movlw		.6					;
			movwf		P1_CNST				;
MD1ST_P1	movlw		.255;				;
			movwf		P2_CNST				;
MD1ST_P2	movlw		.255				;
			movwf		P3_CNST				;
;---------------------------------
MD1ST_P3	bcf			STATUS,RP0			;
			BTFSs		inpS				;
			goto		MD1_WS1				;
			bsf			STATUS,RP0			;
			decfsz		P3_CNST	
			goto		MD1ST_P3			;
			DECFSZ		P2_CNST				;
			goto		MD1ST_P2			;
			DECFSZ		P1_CNST				;
			goto		MD1ST_P1			;
			bcf			STATUS,RP0			;
			GOTO		MD1_S_ER			;
;---------------------------------------------------------			
MD1_WS1		bcf			STATUS,RP0
			BTFSs		inpS
			goto		MD1_WS1
MD1_WS2		BTFSc		inpS
			goto		MD1_WS2
;---------------------------------------------------------
MD1_CNTS	btfsc		inpS			; ���� �� ����� ������������ 1
			goto		MD1_CNTS0		; �� ���� � ������� ������������
			incf		ST,F				; ���� LastTr ������ 0, �� ����������� ������� ��������
			CALL		PAUSE48
			GOTO		MD1_CNTS
;---------------------------------------------------------
MD1_CNTS0	movf		ST,W
			movwf		STC
			call		WRITE_MEM
			call		PAUSE100
			CALL		READ_MEM
			movlw		b'00000011'
			movwf		PORTA
			CALL		PAUSE500
			CALL		PAUSE500			

			clrf		BUT_RES
MODE1_ENDP	goto		END_PRG



MD1_T_ER	bsf		STATUS,RP0	
			movlw	b'10001100'		
			movwf	BLINK_OUTB
			CLRF	BLINK_OUTA
			MOVLW	.6
			MOVWF	BLINK_N
			CALL	BLINK
			CALL	PAUSE500
			goto	MD1_ST
	
MD1_S_ER	bsf		STATUS,RP0	
			movlw	b'00001100'		
			movwf	BLINK_OUTB
			movlw	b'00000011'		
			movwf	BLINK_OUTA
			MOVLW	.6
			MOVWF	BLINK_N
			CALL	BLINK
			CALL	PAUSE500
			goto	END_PRG


END_PRG		CLRF	PORTA
			CLRF	PORTB
			CALL	PAUSE500
			CALL	PAUSE500


;;			movlw		.6
;			bsf			STATUS,RP0	;����� ����� 1
;			bcf			STATUS,RP1
;			movwf		EEADR
;			bsf			EECON1,RD
;			movf		EEDATA,W
;			bcf			STATUS,RP0	;����� ����� 1
;			bcf			STATUS,RP1
;			movwf		REVERS

			CLRF		N
			CLRF		N_WORK
			MOVLW		.2
			MOVWF		DL
			movlw		.3
			movwf		DH
			CLRF		BUT_RES
			CALL		WRITE_MEM
	
			
			GOTO	BEGIN


;===================================================================================================

;������ ��������� ��������:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

;��� ���������---------------------------------------------------------------
EFF1		movlw		.2			;(12)
			movwf		I_CONST		;
			movwf		I
			movlw		.70
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00000000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00000000'
			MOVWF		STAT11
			MOVLW		B'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bcf			outGALOG
			clrf		R
			RETURN					;(27)

;��������---------------------------------------------------------------
EFF2		movlw		.10
			movwf		I_CONST		;
			movwf		I
			movlw		.30
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00000000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00000000'
			MOVWF		STAT11
			MOVLW		B'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN
;�������---------------------------------------------------------------
EFF3		movlw		.20
			movwf		I_CONST		;
			movwf		I
			movlw		.7
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00110011'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		b'00110011'
			MOVWF		STAT11
			MOVLW		b'00110011'
			MOVWF		STAT12
			MOVLW		b'00110011'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN



;������� �� 2 �����������::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
;������� ������������ 2 ������ ������� ����� ����� ---------------------------------------------------------------
EFF4		movlw		.10
			movwf		I_CONST		;
			movwf		I
			movlw		.20
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00110000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00000000'
			MOVWF		STAT11
			MOVLW		B'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;������� ������������ 2 ������ ����� ---------------------------------------------------------------
EFF5		movlw		.20
			movwf		I_CONST		;
			movwf		I
			movlw		.12
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00110000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00000000'
			MOVWF		STAT11
			MOVLW		B'00110000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN


;������� ��������  ������������ (�����, ������) ---------------------------------------------------------------
EFF6		movlw		.14
			movwf		I_CONST		;
			movwf		I
			movlw		.10
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00000011'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00110011'
			MOVWF		STAT11
			MOVLW		B'00110011'
			MOVWF		STAT12
			MOVLW		B'00110011'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;������� ����� ����� ������, ������� ������ ����� ������ ����� ---------------------------------------------------------------
EFF7		movlw		.16
			movwf		I_CONST		;
			movwf		I
			movlw		.12
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00100000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00010000'
			MOVWF		STAT11
			MOVLW		B'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;������� ����� 4 ����, ������ 4 ���� ����� ---------------------------------------------------------------
EFF8		movlw		.8
			movwf		I_CONST		;
			movwf		I
			movlw		.14
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00100000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00010000'
			MOVWF		STAT11
			MOVLW		B'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;������� �����-������, 5 ��� �������������� ---------------------------------------------------------------
EFF9		movlw		.16
			movwf		I_CONST		;
			movwf		I
			movlw		.20
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00100001'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00000000'
			MOVWF		STAT11
			movlw		b'00100001'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN


;������������� 4 ������������:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

;��� ��������---------------------------------------------------------------
EFF10		movlw		.2
			movwf		I_CONST		;
			movwf		I
			movlw		.50
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'11111111'
			movwf		STAT
			MOVWF		STAT10
			movlw		b'11111111'
			MOVWF		STAT11
			movlw		b'11111111'
			MOVWF		STAT12
			movlw		b'11111111'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN



;���������� ���� ���---------------------------------------------------------------
EFF11		movlw		.2
			movwf		I_CONST		;
			movwf		I
			movlw		.23
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'11110000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'11110000'
			MOVWF		STAT11
			movlw		b'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;���������� ���� ��� ����� ������---------------------------------------------------------------
EFF12		movlw		.20
			movwf		I_CONST		;
			movwf		I
			movlw		.12
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'11110000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00000000'
			MOVWF		STAT11
			movlw		b'11110000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;1-3-2-4- �� 2 ����---------------------------------------------------------------
EFF13		movlw		.4
			movwf		I_CONST		;
			movwf		I
			movlw		.20
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00010000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'10000000'
			MOVWF		STAT11
			MOVLW		B'00100000'
			MOVWF		STAT12
			MOVLW		B'01000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;*������� �����, ������� ��������� ������������ �� 3 ����---------------------------------------------------------------
EFF14		movlw		.6
			movwf		I_CONST		;
			movwf		I
			movlw		.18
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00110011'
			movwf		STAT
			MOVWF		STAT10
			movlw		b'11110011'
			MOVWF		STAT11
			movlw		b'00110011'
			MOVWF		STAT12
			movlw		b'11110011'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN
;������� �����, ������� ��������� ����������� �� ������ � ���������� ������---------------------------------------------------------------
EFF15		movlw		.2
			movwf		I_CONST		;
			movwf		I
			movlw		.15
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'01110011'
			movwf		STAT
			MOVWF		STAT10
			movlw		b'00110011'
			MOVWF		STAT11
			movlw		b'10110011'
			MOVWF		STAT12
			movlw		b'00110011'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;������� �����, ������� ��������� ����������� ������� ��� �����---------------------------------------------------------------
EFF16		movlw		.2
			movwf		I_CONST		;
			movwf		I
			movlw		.20
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'01111011'
			movwf		STAT
			MOVWF		STAT10
			movlw		b'01111011'
			MOVWF		STAT11
			movlw		b'01111011'
			MOVWF		STAT12
			movlw		b'01111011'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN
;������ ����� ������ �����---------------------------------------------------------------
EFF17		movlw		.10
			movwf		I_CONST		;
			movwf		I
			movlw		.18
			movwf		R1_CONST	;
			movwf		R1
			movlw		B'10100000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00000000'
			MOVWF		STAT11
			MOVLW		B'01010000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN
;������ ����� ������ ����� ��������---------------------------------------------------------------
EFF18		movlw		.10
			movwf		I_CONST		;
			movwf		I
			movlw		.20
			movwf		R1_CONST	;
			movwf		R1
			movlw		B'01011111'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'10101111'
			MOVWF		STAT11
			MOVLW		B'11111111'
			MOVWF		STAT12
			MOVLW		B'11111111'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN


;����������� ������� ������� �� 3 �����, ����� ---------------------------------------------------------------
EFF19		movlw		.6
			movwf		I_CONST		;
			movwf		I
			movlw		.20
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'11000000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		B'00110000'
			MOVWF		STAT11
			MOVLW		B'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN
;����� � ����, ������ � ���� ---------------------------------------------------------------
EFF20		movlw		.6
			movwf		I_CONST		;
			movwf		I
			movlw		.30
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'01100000'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		b'10010000'
			MOVWF		STAT11
			MOVLW		B'00000000'
			MOVWF		STAT12
			MOVLW		B'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;������� ������, ������������� �������� ---------------------------------------------------------------
EFF21		movlw		.10
			movwf		I_CONST		;
			movwf		I
			movlw		.20
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00111111'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		b'11001111'
			MOVWF		STAT11
			MOVLW		b'00111111'
			MOVWF		STAT12
			MOVLW		b'11001111'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN
;������� �� ������, ����������� ������������ ---------------------------------------------------------------
EFF22		movlw		.6
			movwf		I_CONST		;
			movwf		I
			movlw		.25
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00100001'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		b'11000000'
			MOVWF		STAT11
			MOVLW		b'00000000'
			MOVWF		STAT12
			MOVLW		b'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;������� ����� ����� �������---------------------------------------------------------------
EFF23		movlw		.2
			movwf		I_CONST		;
			movwf		I
			movlw		.30
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00010100'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		b'10000010'
			MOVWF		STAT11
			MOVLW		b'00000000'
			MOVWF		STAT12
			MOVLW		b'00000000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN
;������� ����� ���� ����---------------------------------------------------------------
EFF24		movlw		.2
			movwf		I_CONST		;
			movwf		I
			movlw		.30
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00010100'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		b'10000010'
			MOVWF		STAT11
			MOVLW		b'10000100'
			MOVWF		STAT12
			MOVLW		b'00010000'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

;�������� ��������� �������---------------------------------------------------------------
EFF25		movlw		.2
			movwf		I_CONST		;
			movwf		I
			movlw		.30
			movwf		R1_CONST	;
			movwf		R1
			movlw		b'00011010'
			movwf		STAT
			MOVWF		STAT10
			MOVLW		b'01000000'
			MOVWF		STAT11
			MOVLW		b'01011000'
			MOVWF		STAT12
			MOVLW		b'00000010'
			MOVWF		STAT13
			bsf			outGALOG
			clrf		R
			RETURN

			END


